package com.example.hello_mate

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class ConnectedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connected)
    }
}