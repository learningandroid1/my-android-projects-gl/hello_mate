package com.example.hello_mate

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
    }

    fun toConnected(view: android.view.View) {
        val perehod = Intent(this, ConnectedActivity::class.java)
        startActivity(perehod)
    }
}